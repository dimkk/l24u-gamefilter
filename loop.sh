# !/bin/sh
ulimit -n 65535

while :;
do
	java -server -Dfile.encoding=UTF-8 -Xmx1G -cp "./lib/*" io.m0nster.filter.Starter
	[ $? -ne 1 ] && break
done