@echo off
java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5007 -XX:+AggressiveOpts -XX:+UseFastAccessorMethods -XX:+AlwaysPreTouch -XX:+RelaxAccessControlCheck -XX:+UseBiasedLocking -XX:MaxGCPauseMillis=25 -XX:ParallelGCThreads=2 -cp ./l2game-filter.jar;./lib/* io.m0nster.filter.Starter
@pause