package io.m0nster.filter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.LocalTime;
import java.util.Properties;
import static java.lang.System.getenv;

/**
 * @author PointerRage
 *
 */
public class Config {
	public static void load(File file) throws UncheckedIOException {
		Properties p = new Properties();
		try (FileInputStream fis = new FileInputStream(file)) {
			p.load(fis);
		} catch(IOException e) {
			throw new UncheckedIOException(e);
		}

		Integer lsPort = Integer.parseInt(!getenv("filter.game.port").isEmpty() ? getenv("filter.game.port") : "0");

		Integer filterEnvPort = Integer.parseInt(!getenv("filter.port").isEmpty() ? getenv("filter.port") : "0") ;

		gameserverAddress = !getenv("filter.game.address").isEmpty() ? getenv("filter.game.address") :  p.getProperty("filter.game.address", "78.46.107.153");
		gameserverPort = lsPort == 0 ? Integer.parseInt(p.getProperty("filter.game.port", "7777")) : lsPort;
		
		filterAddress = !getenv("filter.address").isEmpty() ? getenv("filter.address") :  p.getProperty("filter.address", "37.204.241.175");
		filterPort = filterEnvPort == 0 ? Integer.parseInt(p.getProperty("filter.port", "9000")) : filterEnvPort;
		
		readWriteThreads = Integer.parseInt(getenv("threads"));
		
		restartTime = LocalTime.parse(p.getProperty("filter.restart.time", "3:00"));
	}
	
	private static String gameserverAddress;
	private static int gameserverPort;
	
	private static String filterAddress;
	private static int filterPort;
	
	private static int readWriteThreads;
	
	private static LocalTime restartTime;
	
	public static String getGameserverAddress() {
		return gameserverAddress;
	}
	
	public static int getGameserverPort() {
		return gameserverPort;
	}
	
	public static String getFilterAddress() {
		return filterAddress;
	}
	
	public static int getFilterPort() {
		return filterPort;
	}
	
	public static int getReadWriteThreads() {
		return readWriteThreads;
	}
	
	public static LocalTime getRestartTime() {
		return restartTime;
	}
}
